const fs = require('fs')
const path = require('path')
const webpack = require('webpack')

const { getSharedConf } = require('./federation')
const resolve = dir => path.resolve(__dirname, dir)

module.exports = (parcel) => {
  const { NODE_ENV, DEV_PARCELS } = process.env
  const isEnvProduction = NODE_ENV === 'production'
  const staticWebServer = 'http://192.168.60.29:3000'

  const allParcels = fs.readdirSync(resolve('parcels'))
    .map(dir => {
      const fedConf = resolve(`parcels/${dir}/federation.js`)
      return fs.existsSync(fedConf) ? require(fedConf) : null
    })
    .filter(Boolean)
  const devParcels = (DEV_PARCELS || '').split(',')
  const allRemotes = allParcels.reduce((conf, parcel) => {
    return Object.assign(conf, {
      [`@${parcel.name}`]: `${entryName(parcel)}@${entryPath(parcel)}/remoteEntry.js`
    })
  }, {})
  const devServerProxy = allParcels.reduce((conf, parcel) => {
    const mid = devParcels.includes(parcel.name)
      ? {
        target: `http://localhost:${parcel.port}`,
        pathRewrite: { [`^${entryPath(parcel)}`]: '' },
      }
      : { target: staticWebServer }

    return Object.assign(conf, { [`${entryPath(parcel)}/`]: mid })
  }, {})

  require('events').EventEmitter.setMaxListeners(1000000)// Suppress MaxListenersExceededWarning

  return {
    // transpileDependencies: true,
    productionSourceMap: true,
    parallel: parcel.isVue3,
    publicPath: isEnvProduction ? `${entryPath(parcel)}/` : `http://localhost:${parcel.port}`, // root 应用不设置
    outputDir: resolve(`dist${entryPath(parcel)}`),
    indexPath: resolve('dist/index.html'),
    integrity: isEnvProduction,
    lintOnSave: false, // ScriptSetupPlugin 在 eslint 前执行，导致控制台输出 eslint 错误，作者暂时没有解决
    configureWebpack: {
      devtool: 'source-map', // 这里为方便调试，生产记得关闭
      optimization: {
        splitChunks: false,
      },
      devServer: {
        port: parcel.port,
        historyApiFallback: true, // root 应用需要设置
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
        proxy: devServerProxy,
      },
      resolve: {
        alias: {
          // '@': resolve(`parcels/${parcel.name}/src`)
        }
      },
      plugins: [
        // vue2 setup script
        !parcel.isVue3 && require('unplugin-vue2-script-setup/webpack').default({}),

        // 核心
        new webpack.container.ModuleFederationPlugin({
          name: entryName(parcel),
          filename: 'remoteEntry.js',
          exposes: parcel.exposes,
          shared: {
            ...parcel.shared,
            ...getSharedConf(parcel.isVue3)
          },
          remotes: allRemotes,
        }),
      ].filter(Boolean),
    },
    chainWebpack: config => {
      // 统一 html 入口
      config.plugin('html').tap(args => {
        args[0].template = resolve('./index.html')
        return args
      })
      // 仅 base 需要构建 html
      if (!isBaseApp(parcel) && isEnvProduction) {
        config.plugins.delete('html')
      }
    },
  }
};

function entryName(parcel) {
  return `_parcel_${parcel.name.replace(/-/g, '_').toLowerCase()}`
}

function entryPath(parcel) {
  return `/parcels/${parcel.name}`
}

function isBaseApp(parcel) {
  return parcel.name === 'vue3'
}
