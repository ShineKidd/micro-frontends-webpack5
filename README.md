# 基于 Webpack5 Module Federation 实现微前端

此项目演示如何将一个 Vue2 历史项目，拆分成微前端应用，渐进升级 Vue3。

思路：
  1. 编写一个 Vue3 宿主应用（后续 Vue2 代码逐步向此应用迁移）
  2. 将历史应用 Vue2 作为一个**路由组件**渲染在 Vue3 宿主中
  3. 渐进升级：将历史应用 Vue2 部分路由拆分出来升级为 Vue3 写法，作为微模块 vue3-parcel 进行构建
  4. 可选：将历史应用 Vue2 拆分出多个 vue2-parcel 单独构建，以达到分解大型历史应用的目的。后续再每个 vue2-parcel 升级为 vue3-parcel
  5. 最终，历史应用 Vue2 全部重构为 vue3-parcel ，即完成技术升级。


注意：实际项目中的场景非常复杂，欢迎探讨。


目录介绍

```bash
├─ parcels
│  ├─ vue2 # vue2 宿主应用，在 vue3 中渲染
│  ├─ vue2-parcel # vue2 微模块，接入到 vue2 宿主应用
│  ├─ vue3 # vue3 vue3 宿主应用
│  └─vue3-parcel # 微模块，接入到 vue3 宿主应用
├─ scripts # cli 执行脚本
├─ vue-demi # 修复 .mjs 中引用 Vue 打包错误
```

引用关系

```
vue2-parcel    vue3-parcel
    |              |
    V              V
   vue2  ------>  vue3
```

## 启动

首次启动，按如下顺序执行，preview 起来后，只需执行步骤 4 即可

1. `pnpm i`
2. `pnpm build`
3. `pnpm preview`
4. `pnpm dev`


