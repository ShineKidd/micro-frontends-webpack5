const minimist = require("minimist");
const { execSync } = require('child_process')

const args = minimist(process.argv.slice(2))

const { b: BRANCH, x: CI_COMMIT_BRANCH, s: STAGE, p: PARCEL } = args

console.log('------- script ci ---------')
console.log('CI_MERGE_REQUEST_TARGET_BRANCH_NAME:', BRANCH)
console.log('CI_COMMIT_BRANCH:', CI_COMMIT_BRANCH)
console.log('STAGE:', STAGE)
console.log('PARCEL:', PARCEL)
console.log('------- script ci ---------')

if (!BRANCH || !STAGE || !PARCEL) {
  console.warn('未指定 ENV STAGE PARCEL 参数');
  process.exit(1);
}

switch(STAGE) {
  case 'test':
    runTest();
  break;
  case 'build':
    runBuild();
  break;
  case 'deploy':
    runDeploy();
  break;
  case 'rollback':
    runRollback();
  break;
}

async function runTest() {
  console.log('test pass!')
}

async function runBuild() {
  /** dev | test | pre | production */
  const VUE_APP_ENV = BRANCH === 'prod' ? 'production' : BRANCH
  execSync(`./node_modules/.bin/cross-env VUE_APP_ENV=${VUE_APP_ENV} pnpm -C parcels/${PARCEL} build`, { stdio: 'inherit' })
}

async function runDeploy() {
  console.log('todo')
}

async function runRollback() {
  console.log('todo')
}