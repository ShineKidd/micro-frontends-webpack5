const fs = require('fs');
const path = require('path');
const { MultiSelect } = require('enquirer');
const { execSync } = require('child_process');
const minimist = require('minimist')

main().catch(e => {});

async function main() {
  const args = minimist(process.argv.slice(2))
  const cmd = args.cmd || 'serve'

  const parcelDirs = fs.readdirSync(path.resolve(__dirname, '../parcels'));
  const parcelsToDev = await new MultiSelect({
    name: 'parcelsToDev',
    limit: parcelDirs.length,
    message: '选择要进行开发的模块',
    choices: [...parcelDirs],
  }).run();

  if (!parcelsToDev.length) return

  let command = `cross-env DEV_PARCELS=${parcelsToDev} concurrently -p \"[{name}]\" -c \"green,yellow,blue,magenta,cyan,white,gray\" -n \"${parcelsToDev}\"`
  for (const parcel of parcelsToDev) {
    command += ` \"pnpm -C parcels/${parcel} ${cmd}\"`
  }

  execSync(command, { stdio: 'inherit' });
}

module.exports = main
