module.exports.getSharedConf = function (isVue3) {
  return isVue3
    ? {
      vue: {
        shareKey: 'vue3',
        singleton: true,
        strictVersion: true,
        requiredVersion: '^3.2.36',
      },
      'vue-router': {
        shareKey: 'vue-router4',
        singleton: true,
        strictVersion: true,
        requiredVersion: '^4.0.15',
      },
      'vue-demi': {
        shareKey: 'vue-demi3',
        singleton: true,
        strictVersion: true,
        requiredVersion: '^0.13.1',
      },
      'pinia': {
        shareKey: 'pinia3',
        singleton: true,
        strictVersion: true,
        requiredVersion: '^2.0.14',
      },
    } : {
      vue: {
        shareKey: 'vue2',
        singleton: true,
        strictVersion: true,
        requiredVersion: '^2.6.14',
      },
      'vue-router': {
        shareKey: 'vue-router3',
        singleton: true,
        strictVersion: true,
        requiredVersion: '^3.5.1',
      },
      '@vue/composition-api': {
        singleton: true,
        strictVersion: true,
        requiredVersion: '^1.4.0',
      },
      'vue-demi': {
        shareKey: 'vue-demi2',
        singleton: true,
        strictVersion: true,
        requiredVersion: '^0.13.1',
      },
      'vuex': {
        singleton: true,
        strictVersion: true,
        requiredVersion: '^3.3.0',
      },
      'pinia': {
        shareKey: 'pinia2',
        singleton: true,
        strictVersion: true,
        requiredVersion: '^2.0.9',
      }
    }
}
