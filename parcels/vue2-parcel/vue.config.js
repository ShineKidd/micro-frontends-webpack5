const vueConfigBase = require('../../vue.config.base')
const federation = require('./federation')

module.exports = () => {
  return vueConfigBase(federation)
}
