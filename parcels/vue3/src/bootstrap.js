import { createPinia } from 'pinia'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

console.log('vue3 pinia start')

createApp(App)
.use(router)
.use(createPinia())
.mount('#app')

console.log('vue3 pinia end')
