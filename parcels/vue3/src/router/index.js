import { createRouter, createWebHistory } from "vue-router";
import Home from '../views/Home.vue'
import Vue2 from '../views/Vue2.vue'
import { routes } from '@vue3-parcel/router'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      component: Home,
    },
    {
      path: '/vue2',
      component: Vue2,
    },
    ...routes,
  ]
})

export default router