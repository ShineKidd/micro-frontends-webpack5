const pkg = require('./package.json')

module.exports = {
  name: 'vue3',
  isVue3: true,
  port: 9030,
  exposes: {
    './stores': './src/stores/index.js',
    './bootstrap': './src/bootstrap.js',
  },
  shared: {
    ...pkg.dependencies,
  },
}
