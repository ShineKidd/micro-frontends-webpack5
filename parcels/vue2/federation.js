const pkg = require('./package.json')

module.exports = {
  name: 'vue2',
  isVue3: false,
  port: 9020,
  exposes: {
    './App': './src/App.js',
    './utils': './src/utils/index.js',
  },
  shared: {
    ...pkg.dependencies,
  },
}
