import Vue from "vue";
import VueRouter from "vue-router";
import Home from '../views/Home.vue'
import About from '../views/About.vue'
// import { routes } from '../packages/parcel/router'
import { routes } from '@vue2-parcel/router'

// console.log(routes)

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: '/vue2',
  routes: [
    {
      path: '/',
      component: Home,
    },
    {
      path: '/about',
      component: About,
    },
    ...routes,
  ]
})

export default router