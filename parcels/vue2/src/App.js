import { createApp } from 'vue-demi'
import { createPinia, PiniaVuePlugin } from 'pinia'
import App from './App.vue'
import router from './router'
import store from './store'

console.log('vue2 bootstrap')
const app = createApp({
  router,
  store,
  pinia: createPinia(),
  render: h => h(App),
})
app.use(PiniaVuePlugin)
app.config.productionTip = false


export default {
  mount: app.mount,
  unmount: app.unmount,
  router,
}
