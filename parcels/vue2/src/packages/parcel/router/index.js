import List from '../views/List.vue'
import Report from '../views/Report.vue'

export const routes = [
  {
    path: '/list',
    component: List,
  },
  {
    path: '/report',
    component: Report,
  },
]