const pkg = require('./package.json')

module.exports = {
  name: 'vue3-parcel',
  isVue3: true,
  port: 9031,
  exposes: {
    './router': './src/router/index.js',
  },
  shared: {
    ...pkg.dependencies,
  },
}
